# Narrative Visualization of Sequential Data Models

The current project was part of a master thesis research for a data science degree. The work was done in collaboration with Siemens Mobility GmbH Data Analytics Center and TU Berlin DIMA group. The PDF document of the thesis is included for more details.

## Environment
To run the app first create virtual environment and install dependencies.
```
$ pip install virtualenv
$ virtualenv -p python3 venv
$ source venv/bin/activate
$ pip install -r requirements.txt
```
To deactivate venv run
```
$ deactivate
```

PyCharm is the recommended IDE.

## Running the Visualizations

Script path
```
/app/index.py
```
Working directory
```
/app
```

The visualizations take <1 minute to load.
Chromium browser was used to interact with the visualizations. Issues might occur in other browsers as the were not tested.
