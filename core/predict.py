import pandas as pd
from sklearn.externals import joblib
import json

def main():
    rfc = joblib.load('data/rfc_no_downsample_31_max_depth5.pkl')
    dtrees = rfc.estimators_
    data = pd.read_csv("data/50_sessions_with_window_stats.csv")
    jsons = []

    for d in data.values:

        features = d[4:]
        if pd.notna(features[5]):
            j = {}
            j['datetime'] = d[0]
            j['global_session_id'] = d[1]
            j['speed_odom'] = d[2]
            j['diff_opgs'] = d[3]
            features= features.reshape(1, -1)
            j['predict'] = int(rfc.predict(features)[0])
            j['predict_proba'] = rfc.predict_proba(features)[0].tolist()
            tree_estimations = []
            for idx, dtree in enumerate(dtrees):
                tree = {}
                tree['id'] = idx
                tree['predict_proba'] =dtree.predict_proba(features)[0].tolist()
                tree['predict'] = float(dtree.predict(features)[0])
                tree['leaf_id'] = int(dtree.apply(features)[0])
                tree_estimations.append(tree)

            j['trees'] = tree_estimations
            jsons.append(j)

    with open('data/predictions31.json', 'w') as outfile:
        json.dump(jsons, outfile)


if __name__ == "__main__":
    main()

    feature_names = []
    dtree = 1

    from sklearn.externals.six import StringIO
    from IPython.display import Image
    from sklearn.tree import export_graphviz
    import pydotplus

    def draw_tree(dtree, feature_names):
        dot_data = StringIO()
        export_graphviz(dtree, out_file=dot_data,
                        filled=True, rounded=True,
                        special_characters=True,
                        feature_names=feature_names)

        return pydotplus.graph_from_dot_data(dot_data.getvalue())


    Image(draw_tree(dtree, feature_names).create_png())