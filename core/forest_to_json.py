from sklearn.externals import joblib
import json

def main():
    features = ['avg_speed',
                'max_speed',
                'min_speed',
                'avg_accel',
                'max_accel',
                'min_accel']

    labels = [False, True]

    rfc = joblib.load('data/rfc_no_downsample_31_max_depth5.pkl')
    dtrees = rfc.estimators_
    jsons = []
    for idx, dtree in enumerate(dtrees):
        tree_json = get_json_for_tree(dtree, features, labels)
        tree_json['id'] = idx
        jsons.append(tree_json)

    with open('data/trees31.json', 'w') as outfile:
        json.dump(jsons, outfile)


def get_json_for_tree(clf, features, labels, node_index=0):
    node = {}
    if clf.tree_.children_left[node_index] == -1:
        count_labels = zip(clf.tree_.value[node_index, 0], labels)
        total = 0
        max_label = ''
        for count, label in count_labels:
            total += int(count)
            node[str(label)] = int(count)

            if (max_label == '' or node[str(label)] > node[max_label]):
                max_label = str(label)

        node['total'] = total
        node['winner'] = max_label
        node['leaf_id'] = int(node_index)
        if (total > 0):
            node['accuracy'] = node[max_label] * 1.0 / total

    else:
        feature = features[clf.tree_.feature[node_index]]
        threshold = clf.tree_.threshold[node_index]
        node['feature'] = feature
        node['threshold'] = threshold
        node['name'] = '{} > {}'.format(feature, threshold)
        node['node_index'] = int(node_index)
        count_labels = zip(clf.tree_.value[node_index, 0], labels)
        total = 0
        max_label = ''
        for count, label in count_labels:
            total += int(count)
            node[str(label)] = int(count)

            if (max_label == '' or node[str(label)] > node[max_label]):
                max_label = str(label)

        node['total'] = total
        node['accuracy'] = node[max_label] * 1.0 / total
        node['winner'] = max_label
        left_index = clf.tree_.children_left[node_index]
        right_index = clf.tree_.children_right[node_index]
        right_child = get_json_for_tree(clf, features, labels, right_index)
        left_child = get_json_for_tree(clf, features, labels, left_index)

        node['children'] = [right_child,
                            left_child]
    return node


if __name__ == "__main__":
    main()
