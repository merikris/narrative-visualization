import pandas as pd
import json
from sklearn.tree import DecisionTreeClassifier


def main():
    window_stats = pd.read_csv("data/window_stats_downsampled.csv")

    feature_names = ['avg_speed',
                     'max_speed',
                     'min_speed',
                     'avg_accel',
                     'max_accel',
                     'min_accel']

    target_name = 'is_anomaly'
    labels = [True, False]

    jsons = {}
    for feature in feature_names:
        f = [feature]
        dtree = create_tree(window_stats, f, target_name)
        jsons[feature] = rules(dtree, f,labels)

    with open('data/histogram_boundaries.json', 'w') as outfile:
        json.dump(jsons, outfile)


def create_tree(df, feature_names,target_name):
    target = df[target_name]
    features = df[feature_names]
    dtree=DecisionTreeClassifier(criterion='entropy',max_depth=1,min_samples_split=500,min_samples_leaf=200, class_weight='balanced')
    dtree.fit(features,target)
    return dtree

def rules(clf, features, labels, node_index=0):

    node = {}
    if clf.tree_.children_left[node_index] == -1:  # indicates leaf
        count_labels = zip(clf.tree_.value[node_index, 0], labels)
        total = 0
        max_label = ''
        for count, label in count_labels:
            total += int(count)
            node[str(label)] = int(count)

            if (max_label == '' or node[str(label)] > node[max_label]):
                max_label = str(label)

        node['total'] = total
        node['winner'] = max_label
        node['leaf_id'] = node_index
        if (total > 0 ):
            node['accuracy'] = node[max_label] * 1.0 / total
        else:
            return None

    else:
        feature = features[clf.tree_.feature[node_index]]
        threshold = clf.tree_.threshold[node_index]
        node['feature']=feature
        node['threshold'] = threshold
        count_labels = zip(clf.tree_.value[node_index, 0], labels)
        total = 0
        max_label = ''
        for count, label in count_labels:
            total += int(count)
            node[str(label)]= int(count)

            if (max_label == '' or node[str(label)] > node[max_label]):
                max_label = str(label)

        left_index = clf.tree_.children_left[node_index]
        right_index = clf.tree_.children_right[node_index]
        right_child = rules(clf, features, labels, right_index)
        left_child = rules(clf, features, labels, left_index)

        if right_child != None and left_child != None:
            node['right'] = right_child['winner']
            node['left'] = left_child['winner']
            node['accuracy'] = ((right_child[right_child['winner']] +  left_child[left_child['winner']]) * 1.0) / total
    return node



if __name__ == "__main__":
    main()