import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.externals import joblib
from sklearn.metrics import accuracy_score
from sklearn.model_selection import cross_val_score
from sklearn.metrics import confusion_matrix


def main():
    data, feature_names, target_name = prepare_data()
    rfc = train_classifier(data, feature_names, target_name)
    joblib.dump(rfc, 'data/rfc_no_downsample_31_max_depth5.pkl')

def prepare_data():
    window_stats_init = pd.read_csv("data/labeled_window_stats.csv")

    # transform categorical train type data to binary
    train_type_cat = pd.get_dummies(window_stats_init['train_type'])
    window_stats = pd.concat([window_stats_init, train_type_cat], axis=1)

    feature_names = ['avg_speed',
                     'max_speed',
                     'min_speed',
                     'avg_accel',
                     'max_accel',
                     'min_accel']

    target_name = 'is_anomaly'

    window_stats_anomaly = window_stats.loc[window_stats[target_name] == True]
    window_stats_normal_downsampled = window_stats.loc[window_stats[target_name] == False].sample(n=window_stats_anomaly.shape[0])
    window_stats_downsampled = pd.concat([window_stats_normal_downsampled, window_stats_anomaly], axis=0)

    print(window_stats_anomaly.shape[0])
    print(window_stats_init.shape[0])

    return window_stats_init, feature_names, target_name

def train_classifier(data, feature_names, target_name):
    X_train, X_test, y_train, y_test = train_test_split(data[feature_names], data[target_name], test_size=0.2, stratify=data[target_name])
    rfc = RandomForestClassifier(n_estimators=31, class_weight='balanced', max_depth=5)
    scores = cross_val_score(rfc, data[feature_names], data[target_name], cv=5)
    accuracy = scores.mean()
    print(scores)
    print(accuracy)
    print("Accuracy: %0.3f (+/- %0.3f)" % (scores.mean(), scores.std() * 2))

    rfc.fit(X_train, y_train)
    predicted = rfc.predict(X_test)
    cm = confusion_matrix(y_test, predicted)
    accuracy = accuracy_score(y_test, predicted)

    print(accuracy)
    print(cm)

    return rfc

if __name__ == "__main__":
    main()