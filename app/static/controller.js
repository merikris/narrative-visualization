var Controller = function (opts) {
    this.window = opts.window;
    this.items = opts.items;
    this.menu = opts.menu;
    this.height = opts.height;

    this.addNavigation();
};

Controller.prototype.addNavigation = function () {
    var _this = this;
    var nav = this.menu.append('ul').attr('id', 'nav').attr('class', 'list-group list-group-flush');

    this.items.forEach(function (item) {
        nav.append('li').attr('class', 'list-group-item').append('a').attr('href', '#' + item.url).text(item.title)
            .on('click', (function () {
                if (item.state != null) {
                    item.element.setState(item.state)
                }
            }))
    });

    $('#nav').onePageNav();
};