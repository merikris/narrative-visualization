var Introduction = function (opts) {
    this.container = opts.container;
    this.height = opts.height;
    this.width = opts.width;
    this.window = opts.window;

    this.container.style("height", "" + opts.height + "px");
    this.layout();
    this.draw();
};

Introduction.prototype.layout = function () {
    this.container.append("div").attr('class', 'top-padding-extra').append('h1').text('Random Forest for Anomaly Classification');
    var row = this.container.append("div").attr('class', 'row top-padding-extra overflow-scroll').style("height", "" + this.height - 90 + "px");
    this.svgContainer = row.append('div').attr('class', 'col-6 sticky-top');
    this.svg = this.svgContainer.append("svg").attr('height', this.height * 0.7).append("g");

    //Add texts
    this.svgContainer.append('div').text('Random linear classification boundary generation based on labeled data.');
    var text1 = 'The project at hand is part of a master thesis investigating how visualization can increase transparency in machine learning. The current visualization follows the process of analysing labelled speed sensor data for anomalies, developing a random forest classification model to detect them and applying it to unlabelled data.'
    this.textContainer = row.append('div').attr('class', 'text-container-right col-6').style("height", "" + this.height * 0.7 + "px");
    this.textContainer.append('p').text(text1);
};

Introduction.prototype.draw = function () {
    var _this = this;

    this.svg.append("line")
        .attr('class', 'decision-boundary')
        .attr("x1", 0)
        .attr("y1", 0)
        .attr("x2", 0)
        .attr("y2", 0);

    var inter = setInterval(function () {
        _this.update();
    }, 5000);
};

Introduction.prototype.update = function () {
    var height = this.height * 0.7 - 100,
        width = this.width / 2 - 100;

    var x1_rand = Math.random()*width * 0.25,
        y1_rand = Math.random()*height * 0.25,
        y12_rand = Math.random()*height * 0.25 + height / 2;

    var startPointOptions = [[x1_rand, 0], [x1_rand, height], [0, y1_rand], [0, y12_rand]];
    var startPoint = startPointOptions[Math.floor(Math.random() * startPointOptions.length)];
    var x1 = startPoint[0],
        y1 = startPoint[1];

    var x2_rand = Math.random()*width * 0.25 + width / 2,
        y2_rand = Math.random()*height * 0.25 + (y1 > height / 2 ? 0 : height / 2);

    var endPointOptions = [[x2_rand, (y1 > height / 2 ? 0 : height)], [width, y2_rand]];
    var endPoint = endPointOptions[Math.floor(Math.random() * endPointOptions.length)];

    var x2 = endPoint[0],
        y2 = endPoint[1];

    var data = [],
    topColor = "#FBB040",
    bottomColor = "#8DC63F";

    for(var i=0; i < 40; i++){
        var d = {};
        var x,y;
        var search = true;
        while (search) {
            x = Math.random()*(width-100)+50;
            y = Math.random()*(height-100)+50;

            var y_line = (x-x1)/(x2-x1)*(y2-y1) + y1;

            if (Math.abs(y_line-y)>20) {
                search = false;
                d.x = x;
                d.y = y;
                d.top = y > y_line;
                d.correctClassification = Math.random() < 0.8; //approx 80% accuracy

                if ((d.top && d.correctClassification) || !(d.top || d.correctClassification)) {
                    d.color = topColor
                } else {
                    d.color = bottomColor
                }
            }
        }
        data.push(d);
    }

    var nodes = this.svg.selectAll('circle').data(data);

    nodes.enter().append('circle');
    nodes.exit().remove();

    this.svg.selectAll('circle').transition().duration(1000)
        .attr('r', 8)
        .attr('cx', d => d.x)
        .attr('cy', d => d.y)
        .attr('fill', d => d.color);


    var line = this.svg.select('.decision-boundary').transition().duration(1000)
        .attr("x1", x1)
        .attr("y1", y1)
        .attr("x2", x2)
        .attr("y2", y2);
};