var Histogram = function (opts) {

    this.data = opts.data;
    this.container = opts.container;
    this.height = opts.height * 0.7;
    this.width = opts.width;
    this.window = opts.window;
    this.thresholds = opts.thresholds;

    this.container.style("height", "" + opts.height + "px");

    this.layout();
    this.prepareData();
    this.draw();
};

Histogram.prototype.draw = function () {
    this.addButtons();
    this.addAxes();
    this.addLegend();
    this.addData();
    // this.updateOnScroll();
};

Histogram.prototype.layout = function () {
    this.container.append("div").attr('class', 'top-padding-extra').append('h1').text('Histogram');
    var row = this.container.append("div").attr('class', 'row top-buffer overflow-scroll').style("height", "" + this.height / 0.7 - 90 + "px");
    this.textContainer = row.append('div').attr('class', 'col-4 text-container').attr('height', this.height - this.height / 16);
    var text = 'Histogram illustrates the distribution of anomaly and no anomaly events across the 6 feature spaces defined. We see that the decision boundary over these one dimensional feature spaces performs only slightly better than random in terms of the classification accuracy.';
    this.textContainer.append('p').text(text);

    this.element = row.append('div').attr('class', 'col-6 top-buffer-extra sticky-top').append("svg").attr('height', this.height).append("g");
    var details = row.append('div').attr('class', 'col-2 top-buffer-extra sticky-top').attr('height', this.height);
    this.buttons = details.append('div').attr("id", "buttons");
    this.legend = details.append('div').attr('class', 'top-buffer-extra').append("svg").attr('height', this.height/2).attr('id', 'legend');

    this.svgWidth = this.width * 5 / 12
};

Histogram.prototype.prepareData = function () {

    this.featureKeys = ['min_speed', 'max_speed', 'avg_speed', 'min_accel', 'max_accel', 'avg_accel'];
    this.selectedKey = this.featureKeys[0];
    this.anomalyKeys = d3.map(this.data, function (d) {
        return d.is_anomaly;
    }).keys();

    var _this = this;
    this.allGroups = {};
    this.scalesX = {};
    this.scalesY = {};
    this.featureKeys.forEach(function (featureKey) {
        var subKeys = _this.subKeysForKey(featureKey);
        _this.scalesX[featureKey] = d3.scaleBand().range([0, _this.svgWidth]).domain(subKeys);
        var grouped = [];
        var maxCount = 0;

        var featureBoundary = _this.thresholds[featureKey].threshold;
        _this.thresholds[featureKey].roundedThreshold = _this.roundThresholds(featureKey, featureBoundary);

        subKeys.forEach(function (subKey) {
            var filtered = _this.data.filter(d => {
                return _this.roundData(featureKey, d) == subKey
            });


            var counts = {};
            var total = filtered.length;
            counts['total'] = total;
            if (maxCount < total) {
                maxCount = total
            }
            _this.anomalyKeys.forEach(function (anomalyKey) {
                var count = filtered.filter(d => {
                    return d.is_anomaly == anomalyKey
                }).length;
                counts[anomalyKey] = count
            });
            grouped.push({
                key: subKey,
                value: counts
            })
        });
        _this.scalesY[featureKey] = d3.scaleLinear().range([_this.height, 0]).domain([0, maxCount * 1.1]);
        _this.allGroups[featureKey] = grouped
    });

    this.selectedData = this.allGroups[this.selectedKey]
};

Histogram.prototype.addButtons = function () {
    var _this = this;
    this.featureKeys.forEach(function (val, i) {
        var group = _this.buttons.append('div').attr('class', 'radio');
        var button = group.append('input').attr('type', 'radio').attr('name', 'selection').attr('value', val).on('click', function () {
            _this.selectedKey = _this.featureKeys[i];
            _this.updateData();
        });

        group.append('label').attr('padding-left', 10).text(val);

        if (i == 0) {
            button.attr('checked', true)
        }
    });
};

Histogram.prototype.addLegend = function () {
        var features = ['anomaly', 'no anomaly'];

    for (var i = 0, len = features.length; i < len; i++) {
        var feature = features[i];
        var g = this.legend.append('g').attr("transform", "translate(0," + (i * 30)+ ")");
        var cl = feature == "anomaly" ? 'bar--anomaly' : 'bar--no-anomaly';
        g.append('rect').attr('class', cl).attr('width', 20).attr('height', 20);
        g.append('text').attr("transform", "translate(25,15)").text(feature);
    }
    this.legend.append('text').attr("transform", "translate(0,100)").text('Classification accuracy:');
    this.accuracyLabel = this.legend.append('text').attr('class', 'pie-label').attr("transform", "translate(0,150)");
};

Histogram.prototype.addAxes = function () {
    var x = this.scalesX[this.selectedKey];
    var y = this.scalesY[this.selectedKey];

    var xAxis = d3.axisBottom(x);
    var yAxis = d3.axisLeft(y);

    this.element.append('g')
        .attr('transform', 'translate(0, ' + (this.height) + ')')
        .attr("class", "axis axis--x")
        .call(xAxis);

    this.element.append('g')
        .attr("class", "axis axis--y")
        .call(yAxis);

    this.element.append("text")
        .attr('class', 'axis-label')
        .attr("transform", "rotate(-90)")
        .attr("y", -70)
        .attr("x", 0 - (this.height / 2))
        .attr("dy", "1em")
        .style("text-anchor", "middle")
        .text("Count");
};

Histogram.prototype.addData = function () {
    var _this = this;
    var x = this.scalesX[this.selectedKey];
    var y = this.scalesY[this.selectedKey];

    this.element.selectAll(".bar--no-anomaly")
        .data(this.selectedData)
        .enter().append('rect').attr("class", "bar bar--no-anomaly no-anomaly")
        .attr("x", 1)
        .attr("transform", function (d) {
            return "translate(" + x(d.key) + "," + y(d.value.False) + ")";
        })
        .attr("width", function (d) {
            return x.bandwidth();
        })
        .attr("height", function (d) {
            return _this.height - y(d.value.False);
        });

    this.element.selectAll(".bar--anomaly")
        .data(this.selectedData)
        .enter().append("rect").attr("class", "bar bar--anomaly anomaly")
        .attr("x", 1)
        .attr("transform", function (d) {
            return "translate(" + x(d.key) + "," + (y(d.value.True) + y(d.value.False) - _this.height) + ")";
        })
        .attr("width", function (d) {
            return x.bandwidth();
        })
        .attr("height", function (d) {
            return _this.height - y(d.value.True);
        });

    //    Add decision threshold
    var decisionBoundary = this.thresholds[this.selectedKey].roundedThreshold;
    this.element.append("line")
        .attr('class', 'decision-boundary')
        .attr("x1", (x(decisionBoundary) + x.bandwidth()))
        .attr("y1", y.range()[0])
        .attr("x2", (x(decisionBoundary) + x.bandwidth()))
        .attr("y2", y.range()[1]);

    this.accuracyLabel.text(this.thresholds[this.selectedKey].accuracy.toLocaleString("en", {style: "percent"}));
};


Histogram.prototype.updateData = function () {
    var _this = this;

    var x = this.scalesX[this.selectedKey];
    var y = this.scalesY[this.selectedKey];
    var selected = this.allGroups[this.selectedKey];

    var xAxis = d3.axisBottom(x);
    var yAxis = d3.axisLeft(y);


    this.element.select(".axis--x").transition().duration(1000)
        .call(xAxis);

    this.element.select(".axis--y").transition().duration(1000)
        .call(yAxis);


    var bars1 = this.element.selectAll(".bar--no-anomaly")
        .data(selected);
    bars1.enter().append('rect').attr("class", "bar bar--no-anomaly");

    this.element.selectAll(".bar--no-anomaly").transition().duration(1000)
        .attr("x", 1)
        .attr("transform", function (d) {
            return "translate(" + x(d.key) + "," + y(d.value.False) + ")";
        })
        .attr("width", function (d) {
            return x.bandwidth();
        })
        .attr("height", function (d) {
            return _this.height - y(d.value.False);
        });

    bars1.exit().remove();

    var bars2 = this.element.selectAll(".bar--anomaly")
        .data(selected);

    bars2.enter().append("rect").attr("class", "bar bar--anomaly");

    this.element.selectAll(".bar--anomaly").transition().duration(1000)
        .attr("x", 1)
        .attr("transform", function (d) {
            return "translate(" + x(d.key) + "," + (y(d.value.True) + y(d.value.False) - _this.height) + ")";
        })
        .attr("width", function (d) {
            return x.bandwidth();
        })
        .attr("height", function (d) {
            return _this.height - y(d.value.True);
        });

    bars2.exit().remove();

//    Add decision threshold
    var decisionBoundary = this.thresholds[this.selectedKey].roundedThreshold;
    var line = this.element.select('.decision-boundary').transition().duration(1000)
        .attr("x1", (x(decisionBoundary) + x.bandwidth()))
        .attr("y1", y.range()[0])
        .attr("x2", (x(decisionBoundary) + x.bandwidth()))
        .attr("y2", y.range()[1]);

    this.accuracyLabel.transition().duration(1200).text(this.thresholds[this.selectedKey].accuracy.toLocaleString("en", {style: "percent"}));
};

Histogram.prototype.roundData = function (key, d) {
    var step = this.stepForKey(key);
    switch (key) {
        case 'min_speed':
        case 'max_speed':
        case 'avg_speed':
            return Math.round(d[key] / step) * step;
            break;
        default:
            return Math.round(Math.round(d[key] / step) * step * 10) / 10
    }
};

Histogram.prototype.roundThresholds = function (key, d) {
    var step = this.stepForKey(key);
    switch (key) {
        case 'min_speed':
        case 'max_speed':
        case 'avg_speed':
            return Math.round(d / step) * step;
            break;
        default:
            return Math.round(Math.round(d / step) * step * 10) / 10
    }
};

Histogram.prototype.subKeysForKey = function (key) {
    var _this = this;

    var extent = d3.extent(this.data, d => _this.roundData(key, d));
    var keys = [extent[0]];
    var step = this.stepForKey(key);
    while (d3.max(keys) < extent[1]) {
        //Math glitch workaround
        var a = Math.round((d3.max(keys) + step) * 10) / 10;
        keys.push(a)
    }

    return keys
};

Histogram.prototype.stepForKey = function (key) {
    switch (key) {
        case 'min_speed':
        case 'max_speed':
        case 'avg_speed':
            return 5;
            break;
        default:
            return 0.4
    }
};

Histogram.prototype.updateOnScroll = function () {

    var startScrollPosition = this.window.scrollTop();
    var previousScrollPosition = startScrollPosition;
    var buttonScale = d3.scaleQuantize().domain([startScrollPosition, startScrollPosition + this.height]).range(this.featureKeys);
    var prevValue = this.selectedKey;
    var _this = this;

    this.window.scroll(function () {
        if (_this.container.classed("hidden")) {
            return
        }

        var currentScrollPosition = _this.window.scrollTop();

        if (prevValue != buttonScale(currentScrollPosition)) {
            prevValue = buttonScale(currentScrollPosition);
            _this.selectedKey = prevValue;
            _this.buttons.selectAll('button').attr('class', 'stat btn-block');
            _this.buttons.selectAll('button').each(function (val, i) {
                if (_this.featureKeys[i] == _this.selectedKey) {
                    $(this).addClass("active")
                }
            });

            _this.updateData()
        }

        previousScrollPosition = currentScrollPosition
    })
};