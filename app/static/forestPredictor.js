var ForestPredictor = function (opts) {

    this.data = opts.data;
    this.container = opts.container;
    this.height = opts.height * 0.8;
    this.width = opts.width;
    this.window = opts.window;
    this.timelineData = opts.predictions;

    this.state = 0;
    this.previouState = 0;

    this.container.style("height", "" + opts.height * 1.1 + "px");

    this.prepareLayout();
    this.draw();
    this.updateOnScroll();
};

ForestPredictor.prototype.setState = function (state) {
    if (this.state == state) {
        return
    }
    this.previouState = this.state;
    this.state = state;
    this.updateLayout();
};

ForestPredictor.prototype.prepareLayout = function () {
    this.height1 = 11 * this.height / 16;
    this.height2 = 3 * this.height / 16;
    this.height3 = 1 * this.height / 16;

    var scrollOutsideWrapper = this.container.append("div").attr('id', 'forestScroll').style("height", "" + 3 * this.height + "px");
    var wrapper = scrollOutsideWrapper.append("div").attr('class', 'sticky-top').attr('id', 'wrapper');
    this.title = wrapper.append('h1').text('Random Forest');
    var row = wrapper.append("div").attr('class', 'row top-buffer').attr('id', 'forestVis').style("height", "" + this.height - this.height / 16 - 90 + "px");

    this.textContainer = row.append("div").attr('class', 'col-3 text-container');
    var element = row.append('div').attr('class', 'col-9');
    this.forest = element.append('svg').attr('class', 'row top-buffer-extra').attr('height', this.height1);

    this.width = this.width * 8 / 12;
    this.width1 = this.width - 75;
    var forestGroup = this.forest.append('g');
    this.nodesWrapper = forestGroup.append('g');
    this.linksWrapper = forestGroup.append('g');
    this.treemap = d3.tree().size([this.height1, this.width]);

    this.pieWrapper = forestGroup.append('g').attr("transform", "translate(" + (this.width - 100) + ",75)");
    this.legend = forestGroup.append('g').attr("transform", "translate(" + (this.width - 150) + ",0)");
    this.pieLabel = this.pieWrapper.attr('class', 'hidden').append('text').attr('class', 'pie-label').attr("x", -25).attr("y", 10);
    this.timeline = element.append('svg').attr('class', 'timeline top-buffer row hidden')
        .attr('height', this.height2).attr("transform", "translate(50,0)");

    this.timeline2 = element.append('svg').attr('class', 'timeline row top-buffer hidden').attr("transform", "translate(50,0)")
        .attr('height', this.height3);

    this.defineSvgPatterns();
    this.defineTexts();
};

ForestPredictor.prototype.defineTexts = function () {
    var text01 = 'The Random Forest consist of decision tree nodes each trained on a subset of the training data. Each circle presents one main node of the decision tree and the most relevant feature for the split. On click the node expands to a decision tree enabling to explore the specific tree in the random forest. ';
    var text02 = 'The accuracy of the Random Forest classification based on the test data set is 95%.';

    var text1 = 'On opening up the decision tree node we can go deeper into the features and decision bounadries on each step. Here the node size is mapped to the number of training data samples classified in every node.';

    var text2 = 'Now we are ready to see the model in action. By sliding the 60s window slider over the timeline graph we can see the Random Forest classification result and the associated probability in the pie chart in top right corner. In addition to that, each tree node border is mapped to the prediction made by the individual tree.';
    var text3 = 'In current view we can go further looking into the decision path for the selected time window. The highlighted path shows how the features of the time window map to the selected decision tree.';

    this.state0_texts = [text01, text02];
    this.state1_texts = [text1];
    this.state2_texts = [text2];
    this.state3_texts = [text3];

    var texts = this.textContainer.selectAll('p').data(this.state0_texts);
    texts.enter().append('p').text(d => d);
    texts.exit().remove();
};

ForestPredictor.prototype.updateLayout = function () {
    if (this.state == 0) {
        this.prediction = null;
        this.pieWrapper.attr('class', 'hidden');
        this.legend.attr('class', 'visible');
        this.timeline.attr('class', 'timeline row top-buffer hidden');
        this.timeline2.attr('class', 'timeline row top-buffer hidden');

        this.roots.forEach(d => this.collapse(d));
        this.links = [];
        this.nodes = [];
        this.updateRootSimulation();
        this.updateForestStyles();

        var texts = this.textContainer.selectAll('p').data(this.state0_texts);
        texts.exit().remove();
        this.textContainer.selectAll('p').text(d => d);
    }
    else if (this.state == 1) {
        this.prediction = null;
        this.pieWrapper.attr('class', 'hidden');
        this.legend.attr('class', 'hidden');
        this.timeline.attr('class', 'timeline row top-buffer hidden');
        this.timeline2.attr('class', 'timeline row top-buffer hidden');

        if (this.nodes.length == 0) {
            var rootNode = this.roots[1];
            this.expand(rootNode);
            this.calculateNodes();
            this.updateTreeMap(rootNode);
        }

        var texts = this.textContainer.selectAll('p').data(this.state1_texts);
        texts.exit().remove();
        this.textContainer.selectAll('p').text(d => d);
    }
    else if (this.state == 2) {
        this.pieWrapper.attr('class', 'visible');
        this.legend.attr('class', 'hidden');
        this.timeline.attr('class', 'timeline row top-buffer visible');
        this.timeline2.attr('class', 'timeline row top-buffer visible');

        this.roots.forEach(d => this.collapse(d));
        this.links = [];
        this.nodes = [];
        this.updateRootSimulation();

        this.timeline.select('.brush').style("pointer-events", "all");
        this.timeline2.select('.brush').style("pointer-events", "all");

        if (this.previouState != 3) {
            let minDate = d3.min(this.timelineData, d => new Date(d.datetime));
            this.timeline.select('.brush').call(this.brush.move, [0, this.x(d3.timeMinute.offset(minDate, 1))]);
        }
        else {
            this.updateForestStyles();
        }

        var texts = this.textContainer.selectAll('p').data(this.state2_texts);
        texts.exit().remove();
        this.textContainer.selectAll('p').text(d => d);
    }
    else if (this.state == 3) {
        this.pieWrapper.attr('class', 'hidden');
        this.legend.attr('class', 'hidden');
        this.timeline.select('.brush').style("pointer-events", "none");
        this.timeline2.select('.brush').style("pointer-events", "none");

        if (this.nodes.length == 0) {
            this.nodeClicked(this.roots[0]);
        }
        var texts = this.textContainer.selectAll('p').data(this.state3_texts);
        texts.exit().remove();
        this.textContainer.selectAll('p').text(d => d);
    }
};

ForestPredictor.prototype.defineSvgPatterns = function () {

    this.noAnomalyColor = '#8DC63F';
    this.anomalyColor = '#FBB040';

    var avg_speed = this.forest
        .append('defs')
        .append('pattern')
        .attr('id', 'avg_speed')
        .attr('patternUnits', 'userSpaceOnUse')
        .attr('width', 8)
        .attr('height', 8);
    avg_speed.append('rect')
        .attr('width', 8)
        .attr('height', 8)
        .attr('x', 0)
        .attr('x', 0)
        .attr('fill', '#00AEEF');
    avg_speed.append('path')
        .attr('d', 'M-2,2 l4,-4 M0,8 l8,-8 M6,10 l4,-4')
        .attr('stroke', '#000000')
        .attr('stroke-width', 2);

    var avg_accel = this.forest
        .append('defs')
        .append('pattern')
        .attr('id', 'avg_accel')
        .attr('patternUnits', 'userSpaceOnUse')
        .attr('width', 8)
        .attr('height', 8);
    avg_accel.append('rect')
        .attr('width', 8)
        .attr('height', 8)
        .attr('x', 0)
        .attr('x', 0)
        .attr('fill', '#EE2A7B');
    avg_accel.append('path')
        .attr('d', 'M-2,2 l4,-4 M0,8 l8,-8 M6,10 l4,-4')
        .attr('stroke', '#000000')
        .attr('stroke-width', 2);

    var max_speed = this.forest
        .select('defs').append('pattern')
        .attr('id', 'max_speed')
        .attr('patternUnits', 'userSpaceOnUse')
        .attr('width', 6)
        .attr('height', 6);
    max_speed.append('rect')
        .attr('width', 6)
        .attr('height', 6)
        .attr('x', 0)
        .attr('x', 0)
        .attr('fill', '#00AEEF');
    max_speed.append('path')
        .attr('d', 'M0,0 l0,6')
        .attr('stroke', '#000000')
        .attr('stroke-width', 4);

    var max_accel = this.forest
        .select('defs').append('pattern')
        .attr('id', 'max_accel')
        .attr('patternUnits', 'userSpaceOnUse')
        .attr('width', 6)
        .attr('height', 6);
    max_accel.append('rect')
        .attr('width', 6)
        .attr('height', 6)
        .attr('x', 0)
        .attr('x', 0)
        .attr('fill', '#EE2A7B');
    max_accel.append('path')
        .attr('d', 'M0,0 l0,6')
        .attr('stroke', '#000000')
        .attr('stroke-width', 4);

    var min_speed = this.forest
        .select('defs').append('pattern')
        .attr('id', 'min_speed')
        .attr('patternUnits', 'userSpaceOnUse')
        .attr('width', 6)
        .attr('height', 6);
    min_speed.append('rect')
        .attr('width', 6)
        .attr('height', 6)
        .attr('x', 0)
        .attr('x', 0)
        .attr('fill', '#00AEEF');
    min_speed.append('path')
        .attr('d', 'M0,0 l6,0')
        .attr('stroke', '#000000')
        .attr('stroke-width', 4);

    var min_accel = this.forest
        .select('defs').append('pattern')
        .attr('id', 'min_accel')
        .attr('patternUnits', 'userSpaceOnUse')
        .attr('width', 6)
        .attr('height', 6);
    min_accel.append('rect')
        .attr('width', 6)
        .attr('height', 6)
        .attr('x', 0)
        .attr('x', 0)
        .attr('fill', '#EE2A7B');
    min_accel.append('path')
        .attr('d', 'M0,0 l6,0')
        .attr('stroke', '#000000')
        .attr('stroke-width', 4);

};

ForestPredictor.prototype.draw = function () {
    var _this = this;

    this.thresholdFormat = d3.format(".1f");
    this.tip = d3.tip()
        .attr('class', 'd3-tip')
        .offset([-10, 0])
        .html(function (d) {
            if (d.feature != null) {
                return d.feature + " : " + _this.thresholdFormat(d.threshold);
            }
            return "Winner: anomaly " + d.winner.toLowerCase();
        });

    this.data.forEach(d => this.collapse(d));
    this.data.forEach(d => this.calculateRandomNodePosition(d));

    this.nodes = [];
    this.links = [];
    this.roots = this.data;
    this.hierarchy = {};
    this.i = 0;
    this.duration = 750;
    this.prediction = null;

    this.updateRootSimulation();
    this.drawTimeline();
    this.addLegend();
};

ForestPredictor.prototype.drawTimeline = function () {
    let minDate = d3.min(this.timelineData, d => new Date(d.datetime));
    this.x = d3.scaleTime()
        .range([0, this.width1])
        .domain([minDate, d3.timeMinute.offset(minDate, 8)])
        .clamp(true);

    this.y = d3.scaleLinear()
        .range([this.height2, 0])
        .domain(d3.extent(this.timelineData, d => +d.speed_odom))
        .clamp(true);

    this.x2 = d3.scaleTime()
        .range([0, this.width1])
        .domain([minDate, d3.timeMinute.offset(minDate, 200)])
        .clamp(true);

    this.y2 = d3.scaleLinear()
        .range([this.height3, 0])
        .domain(d3.extent(this.timelineData, d => +d.speed_odom))
        .clamp(true);

    this.formatSecond = d3.timeFormat("%I:%M:%S");
    this.formatMinute = d3.timeFormat("%I:%M");
    this.xAxis = d3.axisBottom(this.x).tickFormat(this.formatSecond);
    this.xAxis2 = d3.axisBottom(this.x).tickFormat(this.formatMinute);
    this.yAxis = d3.axisLeft(this.y);

    this.timeline.append('g')
        .attr('transform', 'translate(0, ' + this.height2 + ')')
        .attr("class", "axis axis--x")
        .call(this.xAxis);

    this.timeline2.append('g')
        .attr('transform', 'translate(0, ' + this.height3 + ')')
        .attr("class", "axis axis--x")
        .call(this.xAxis2);

    this.timeline.append('g')
        .attr("class", "axis axis--y")
        .call(this.yAxis);

    this.line = d3.line()
        .x(d => this.x(new Date(d.datetime)))
        .y(d => this.y(d.speed_odom));

    this.line2 = d3.line()
        .x(d => this.x2(new Date(d.datetime)))
        .y(d => this.y2(d.speed_odom));

    this.addTimelineData();
    this.addBrush();
};

ForestPredictor.prototype.addTimelineData = function () {
    let sessions = d3.nest()
        .key(function (d) {
            return d.global_session_id;
        })
        .entries(this.timelineData);

    var _this = this;
    sessions.forEach(function (session) {
        _this.timeline
            .append('path')
            .datum(session.values)
            .attr('class', 'line line--speed')
            .attr('d', d => _this.line(d));

        _this.timeline2
            .append('path')
            .datum(session.values)
            .attr('class', 'line line--speed')
            .attr('d', d => _this.line2(d));
    })
};

ForestPredictor.prototype.addBrush = function () {
    let minDate = d3.min(this.timelineData, d => new Date(d.datetime));
    var _this = this;

    this.brush = d3.brushX()
        .extent([
            [0, 0],
            [this.width1, this.height2]
        ])
        .on("brush end", function () {
            if (_this.state == 2) {
                let s = d3.event.selection || _this.x.range();
                let startTime = _this.x.invert(s[0]);
                let endTime = _this.x.invert(s[0]);
                _this.prediction = _this.timelineData.filter(d => {
                    var date = new Date(d.datetime);
                    var diff = (date - startTime) / 1000;

                    return Math.abs(diff) < 1;
                })[0];

                _this.updateForestStyles();
            }
        });

    this.brush2 = d3.brushX()
        .extent([
            [0, 0],
            [this.width1, this.height3]
        ])
        .on("brush end", function () {
            if (_this.state == 2) {
                let s = d3.event.selection || _this.x1.range();
                _this.x.domain(s.map(_this.x2.invert, _this.x2));
                _this.timeline.selectAll(".line").attr("d", _this.line);
                _this.timeline.select(".axis--x").call(_this.xAxis);

                let startTime = _this.x.invert(d3.brushSelection(brushG.node())[0]);
                _this.prediction = _this.timelineData.filter(d => {
                    var date = new Date(d.datetime);
                    var diff = (date - startTime) / 1000;

                    return Math.abs(diff) < 1;
                })[0];

                _this.updateForestStyles();
            }
        });

    var brushG = this.timeline.append("g")
        .attr("class", "brush")
        .call(this.brush).call(this.brush.move, [0, this.x(d3.timeMinute.offset(minDate, 1))]);

    this.timeline2.append("g")
        .attr("class", "brush")
        .call(this.brush2).call(this.brush2.move, [0, this.x2(d3.timeMinute.offset(minDate, 8))]);
};

ForestPredictor.prototype.updateRootForce = function () {
    var _this = this;
    this.simulation = d3.forceSimulation()
        .force("charge", null)
        .on("tick", function (d) {
            _this.nodesWrapper.selectAll('.node').attr("transform", function (d) {
                if (_this.state == 1 || _this.state == 3) {
                    return `translate(${d.x},${d.y})`;
                }
                var r = 20;
                var x = Math.max(r, Math.min(_this.width - r, d.x));
                var y = Math.max(r, Math.min(_this.height1 - r, d.y));
                return `translate(${x},${y})`;
            });
        });

    this.simulation
        .force("forceX", d3.forceX().strength(.1).x(d => d.x_random))
        .force("forceY", d3.forceY().strength(.1).y(d => d.y_random))
        .force("center", d3.forceCenter().x(this.width * .4).y(this.height1 * .5))
        .force("collide", d3.forceCollide().strength(.5).radius(function (d) {
            return 30;
        }).iterations(1));

    this.simulation.alphaTarget(.15).restart();
    this.forest.call(this.tip);
};

ForestPredictor.prototype.updateRootSimulation = function () {
    this.updateRootForce();
    this.simulation.nodes(this.roots);

    var _this = this;

    //links
    var links = this.linksWrapper.selectAll('path').data(this.links);
    links.exit().remove();

    var nodes = this.nodesWrapper.selectAll('.node').data([]);
    var exited = nodes.exit().remove();

    //nodes
    nodes = this.nodesWrapper.selectAll('.node').data(this.roots);
    exited = nodes.exit().remove();
    var enteredNodes = nodes.enter()
        .append('g')
        .attr('class', 'node');

    //bind event handlers
    enteredNodes
        .on('click', d => _this.nodeClicked(d))
        .on('mouseover', _this.tip.show)
        .on('mouseout', _this.tip.hide)
        .call(d3.drag()
            .on("start", d => _this.dragstarted(d))
            .on("drag", d => _this.dragged(d))
            .on("end", d => _this.dragended(d)));


    //channels grandchildren
    var channelsGrandchildren = enteredNodes
        .append("circle").attr('r', 20)
        .attr('fill', d => _this.getFillForNode(d));


    nodes.selectAll('circle').transition().duration(1000).attr('r', 20)
        .attr('fill', d => _this.getFillForNode(d));

    //merge  node groups and style it
    nodes = enteredNodes.merge(nodes);
    nodes.style('cursor', 'pointer');
};

ForestPredictor.prototype.getFillForNode = function (d) {
    if (d.feature != null) {
        return 'url(#' + d.feature + ')'
    }
    return d.winner == "True" ? this.anomalyColor : this.noAnomalyColor;
};

ForestPredictor.prototype.updateForestStyles = function () {
    var _this = this;
    this.nodesWrapper
        .selectAll('circle')
        .attr('class', d => _this.getClassificationClass(d));


    this.updatePie();
};

ForestPredictor.prototype.updatePie = function () {
    if (this.prediction == null || this.prediction.trees == null) {
        this.pieWrapper.attr('class', 'hidden');
        return null
    }

    var _this = this;

    this.pieWrapper.attr('class', 'visible');

    function arcTween(d, index) {
        var i = d3.interpolate(this._current, d);
        if (index === 1)
            this._current = i(0);
        return function (t) {
            return arc(i(t), index);
        };
    }

    var radius = 100;
    var pie = d3.pie()
        .value(function (d) {
            return d;
        })
        .sort(null);

    var arc = d3.arc()
        .innerRadius(radius - 40)
        .outerRadius(radius - 25);


    var path = this.pieWrapper.selectAll("path");
    var data1 = pie(this.prediction.predict_proba);

    path = path.data(data1);

    path
        .transition()
        .duration(500)
        .attrTween("d", arcTween);

    path
        .enter()
        .append("path")
        .attr("fill", function (d, i) {
            return i == 0 ? _this.noAnomalyColor : _this.anomalyColor
        })
        .transition()
        .duration(500)
        .attrTween("d", arcTween);

    path
        .exit()
        .transition()
        .duration(500)
        .remove();


    this.pieLabel.text(this.prediction.predict_proba[0].toLocaleString("en", {style: "percent"}));

};

ForestPredictor.prototype.getClassificationClass = function (d) {
    if (this.prediction == null || this.prediction.trees == null) {
        return null
    }
    var treePrediction = this.prediction.trees.filter(tree => {
        return tree.id == d.id;
    })[0];

    return treePrediction.predict == 1 ? 'anomaly' : 'no-anomaly';
};

ForestPredictor.prototype.getLinkClass = function (d) {
    if (this.prediction == null || this.prediction.trees == null || this.decisionPath == null) {
        return 'link'
    }

    return this.decisionPath.includes(d.data.node_index) ? 'link-path' : 'link';
};

ForestPredictor.prototype.updateTreeMap = function (source) {
    var _this = this;
    // ****************** Nodes section ***************************

    // Update the nodes...
    node = this.nodesWrapper.selectAll('.node')
        .data(this.nodes, function (d) {
            return d.id || (d.id = _this.i);
        });

    // Enter any new modes at the parent's previous position.
    var nodeEnter = node.enter().append('g')
        .attr('class', 'node')
        .attr("transform", function (d) {
            return "translate(" + source.x0 + "," + source.y0 + ")";
        })
        .on('click', d => this.nodeClicked(d))
        .on('mouseover', _this.tip.show)
        .on('mouseout', _this.tip.hide);

    // Add Circle for the nodes
    nodeEnter.append('circle')
        .append("circle").attr('r', d => d.r)
        .attr('fill', d => _this.getFillForNode(d));

    // UPDATE
    var nodeUpdate = nodeEnter.merge(node);

    // Transition to the proper position for the node
    nodeUpdate.transition()
        .duration(this.duration)
        .attr("transform", function (d) {
            return "translate(" + d.x + "," + d.y + ")";
        });

    // Update the node attributes and style
    nodeUpdate.select('circle')
        .attr('r', d => d.r)
        .attr('class', '')
        .attr('fill', d => _this.getFillForNode(d))
        .attr('cursor', 'pointer');

    nodeUpdate.select('text')
        .text(d => d.data.threshold ? _this.thresholdFormat(d.data.threshold) : "");


    // Remove any exiting nodes
    var nodeExit = node.exit().transition()
        .duration(this.duration)
        .attr("transform", function (d) {
            return "translate(" + source.x + "," + source.y + ")";
        })
        .remove();

    // On exit reduce the node circles size to 0
    nodeExit.select('circle')
        .attr('r', 1e-6);

    // On exit reduce the opacity of text labels
    nodeExit.select('text')
        .style('fill-opacity', 1e-6);

    // ****************** links section ***************************

    // Update the links...
    var link = this.linksWrapper.selectAll('path')
        .data(this.links);

    // Enter any new links at the parent's previous position.
    var linkEnter = link.enter().insert('path', "g")
        .attr('d', function (d) {
            var o = {x: source.x0, y: source.y0};
            return diagonal(o, o)
        });

    // UPDATE
    var linkUpdate = linkEnter.merge(link);

    // Transition back to the parent element position
    linkUpdate.transition()
        .duration(this.duration)
        .attr('d', function (d) {
            return diagonal(d, d.parent)
        });

    this.linksWrapper.selectAll('path').attr('class', d => _this.getLinkClass(d));

    // Remove any exiting links
    var linkExit = link.exit().transition()
        .duration(this.duration)
        .attr('d', function (d) {
            var o = {x: source.x, y: source.y};
            return diagonal(o, o)
        })
        .remove();

    // Store the old positions for transition.
    this.nodes.forEach(function (d) {
        d.x0 = d.x;
        d.y0 = d.y;
    });

    // Creates a curved (diagonal) path from parent to the child nodes
    function diagonal(s, d) {

        path = `M ${s.x} ${s.y}
            C ${(s.x + d.x) / 2} ${s.y},
              ${(s.x + d.x) / 2} ${d.y},
              ${d.x} ${d.y}`;

        return path
    }
};

ForestPredictor.prototype.dragstarted = function (d) {
    d.fx = d.x;
    d.fy = d.y;
};

ForestPredictor.prototype.dragged = function (d) {
    d.fx = d3.event.x;
    d.fy = d3.event.y;
};

ForestPredictor.prototype.dragended = function (d) {
    d.fx = null;
    d.fy = null;
};

ForestPredictor.prototype.nodeClicked = function (d) {
    if (d._children == null && d.data._children == null) {
        return
    }

    this.tip.hide(d);

    if (d.children) {
        this.collapse(d);
    } else {
        d.children = d._children;
    }
    if (d.data) {
        if (d.data.children) {
            this.collapse(d.data);
        } else {
            d.data.children = d.data._children;
        }
    }

    if (this.state == 2) {
        this.calculatDecisionPath(d);
        this.expand(d);
    }
    this.calculateNodes();
    if (this.nodes.length > 0) {
        //The opened node was the root node
        if (d.parent == null) {
            if (this.state != 1) {
                this.setState(this.state + 1)
            }
        }

        this.simulation = null;
        this.updateTreeMap(d);
    }
    else {
        this.setState(this.state - 1);
    }
};

ForestPredictor.prototype.calculatDecisionPath = function (d) {
    if (this.prediction == null || this.prediction.trees == null) {
        return null
    }

    var treePrediction = this.prediction.trees.filter(tree => {
        return tree.id == d.id;
    })[0];

    var leaf_id = treePrediction.leaf_id;
    this.decisionPath = null;
    var _this = this;
    var stacks = [];

    var decisionPath = null;
    function findPath(stack, d) {
        //Copy stack without a reference to the array object
        var s = JSON.parse(JSON.stringify(stack));
        s.push(d);
        var children = d.children || d._children;
        if (children != null) {
            children.forEach(function (node) {
                findPath(s, node);
            });
        }
        else if (d.leaf_id == leaf_id) {
            d.node_index = leaf_id;
            decisionPath = s.map(d => d.node_index);
        }
    }

    findPath([], d);
    this.decisionPath = decisionPath;
};

ForestPredictor.prototype.collapse = function (d) {
    var _this = this;
    if (d.children != null) {
        d._children = d.children;
    }
    if (d.children) {
        d.children.forEach(function (child) {
            _this.collapse(child);
        });
        d.children = null;
    }
};

ForestPredictor.prototype.expand = function (d) {
    var _this = this;
    if (d.children == null && (this.prediction == null || this.decisionPath.includes(d.node_index))) {
        d.children = d._children;
    }
    if (d._children) {
        d._children.forEach(function (child) {
            _this.expand(child);
        });
    }
};

ForestPredictor.prototype.calculateNodes = function () {
    var _this = this;
    this.nodes = [];
    this.links = [];
    this.roots.forEach(function (node) {
        if (node.children) {
            var root = d3.hierarchy(node, function (d) {
                return d.children;
            });
            root.x0 = _this.height1 / 2;
            root.y0 = 100;
            root.x = _this.height1 / 2;
            root.y = 100;

            // Assigns the x and y position for the nodes
            var treeData = _this.treemap(root);

            // Compute the new tree layout.
            _this.nodes = treeData.descendants();
            _this.links = treeData.descendants().slice(1);

            //Flip x and y to make it horizontal, adjust depth
            _this.nodes.forEach(function (d) {
                d.threshold = d.data.threshold;
                d.winner = d.data.winner;
                d.feature = d.data.feature;
                d.r = Math.max(Math.sqrt((3.14 * 600 * d.data.total) / 3140000.0), 2);
                let r = d.parent ? d.parent.r : d.r;
                var x = d.x;
                var x0 = d.x0;
                var parent_x = d.parent ? d.parent.x : 0;
                d.x = Math.max((parent_x + 2 * r), parent_x + 120);
                d.x0 = d.y0 + r;
                d.y = x;
                d.y0 = x0;
            });
        }
    });
};

ForestPredictor.prototype.calculateRandomNodePosition = function (d) {
    var x_random = (this.width - 20) * Math.random() + 20;
    var y_random = this.height1 * Math.random();

    //Leave space empty for the prediction label node
    if (x_random >= this.width - 200 && y_random <= 200) {
        this.calculateRandomNodePosition(d)
    }
    else {
        d.x_random = x_random;
        d.y_random = y_random;
    }
};

ForestPredictor.prototype.addLegend = function () {
    var _this = this;
    var featureKeys = ['min_speed', 'max_speed', 'avg_speed', 'min_accel', 'max_accel', 'avg_accel'];

    for (var i = 0, len = featureKeys.length; i < len; i++) {
        var feature = featureKeys[i];
        var g = _this.legend.append('g').attr("transform", "translate(0," + (i * 30) + ")");
        g.append('rect').attr('fill', 'url(#' + feature + ')').attr('width', 20).attr('height', 20);
        g.append('text').attr("transform", "translate(25,15)").text(feature);
    }
};

ForestPredictor.prototype.updateOnScroll = function () {
    var _this = this;
    var f = $('#forest');
    var scrollHeight = 3 * this.height;
    f.scroll(function () {
        var scrollPos = f.scrollTop();
        var state = Math.floor(scrollPos / (scrollHeight / 5));
        _this.setState(state);
    });

    this.window.scroll(function () {

        var scrollBottom = $(document).height() - $(window).height() - $(window).scrollTop();

        if (scrollBottom == 0) {
            _this.container.style("overflow", "scroll");
        }
        else {
            _this.container.style("overflow", "hidden");
        }
    })
};

