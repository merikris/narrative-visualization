var TimeLine = function (opts) {

    this.data = opts.data;
    this.container = opts.container;
    this.height = opts.height;
    this.width = opts.width;
    this.window = opts.window;

    this.container.style("height", "" + opts.height + "px");

    this.prepareLayout();
    this.draw();
};

TimeLine.prototype.prepareLayout = function () {

    this.height = this.height - 90;
    this.height1 = 2 * this.height / 16;
    this.height2 = 3 * this.height / 16;
    this.height3 = 6 * this.height / 16;

    this.width1 = (this.width / 12) * 7;

    this.container.append("div").attr('class', 'top-padding-extra').append('h1').text('Timeline');
    var row = this.container.append("div").attr('class', 'row top-buffer overflow-scroll').style("height", "" + this.height + "px");
    this.textContainer = row.append('div').attr('class', 'col-4 text-container').attr('height', this.height - this.height / 16);

    var text = 'Timeline graph shows the speed change over time in the raw training data. On the right axis the difference of sensor measurements is shown. If the measured difference is greater than 1 km/h the anomaly has occured.';
    this.textContainer.append('p').text(text);

    var element = row.append('div').attr('class', 'col-8 sticky-top').attr('height', this.height - this.height / 16);
    this.timeline1 = element.append('svg').attr('class', 'timeline row').attr('height', this.height1);

    this.timeline2 = element.append('svg').attr('class', 'timeline row top-buffer-extra')
        .attr('height', this.height2);


    this.timeline3 = element.append('svg').attr('class', 'timeline row top-buffer-extra')
        .attr("height", this.height3);

    var gradient = this.timeline3.append("linearGradient")
        .attr("id", "svgGradient")
        .attr("x1", "0%")
        .attr("x2", "0%")
        .attr("y1", "0%")
        .attr("y2", "100%");

    gradient.append("stop")
        .attr("offset", "0%")
        .attr("stop-color", "#FBB040");

    gradient.append("stop")
        .attr("offset", "100%")
        .attr("stop-color", "#8DC63F");
};

TimeLine.prototype.draw = function () {
    this.prepSound();
    this.createScales();
    this.addAxes();
    this.addData();
    this.addBrushes();
    // this.updateOnScroll();
};

TimeLine.prototype.createScales = function () {

    let minDate = d3.min(this.data, d => new Date(d.datetime));
    this.x1 = d3.scaleTime()
        .range([0, this.width1])
        .domain(d3.extent(this.data, d => new Date(d.datetime)))
        .clamp(true);

    this.x2 = d3.scaleTime()
        .range([0, this.width1])
        .domain([minDate, d3.timeMinute.offset(minDate, 30)])
        .clamp(true);

    this.x3 = d3.scaleTime()
        .range([0, this.width1])
        .domain([minDate, d3.timeMinute.offset(minDate, 1)])
        .clamp(true);

    this.x4 = d3.scaleTime()
        .range([0, this.x3(d3.timeSecond.offset(minDate, 60))])
        .domain([minDate, d3.timeSecond.offset(minDate, 60)]);

    this.y1 = d3.scaleLinear()
        .range([this.height1, 0])
        .domain(d3.extent(this.data, d => +d.speed_odom))
        .clamp(true);

    this.y1_2 = d3.scaleLinear()
        .range([this.height1, 0])
        .domain([0, 2])
        .clamp(true);

    this.y2 = d3.scaleLinear()
        .range([this.height2, 0])
        .domain(d3.extent(this.data, d => +d.speed_odom))
        .clamp(true);

    this.y2_2 = d3.scaleLinear()
        .range([this.height2, 0])
        .domain([0, 2])
        .clamp(true);

    this.y3 = d3.scaleLinear()
        .range([this.height3, 0])
        .domain(d3.extent(this.data, d => +d.speed_odom))
        .clamp(true);

    this.y3_2 = d3.scaleLinear()
        .range([this.height3, 0])
        .domain([0, 2])
        .clamp(true)
};

TimeLine.prototype.addAxes = function () {
    this.formatMinute = d3.timeFormat("%I:%M");
    this.formatSecond = d3.timeFormat("%I:%M:%S");
    this.xAxis1 = d3.axisBottom(this.x1).tickFormat(this.formatMinute);
    this.xAxis2 = d3.axisBottom(this.x2).tickFormat(this.formatMinute);
    this.xAxis3 = d3.axisBottom(this.x3).tickFormat(this.formatSecond);

    this.yAxisLeft3 = d3.axisLeft(this.y3);
    this.yAxisRight3 = d3.axisRight(this.y3_2);

    this.timeline1.append('g')
        .attr('transform', 'translate(0, ' + this.height1 + ')')
        .attr("class", "axis axis--x")
        .call(this.xAxis1);

    this.timeline2.append('g')
        .attr('transform', 'translate(0, ' + this.height2 + ')')
        .attr("class", "axis axis--x")
        .call(this.xAxis2);

    this.timeline3.append('g')
        .attr('transform', 'translate(0, ' + this.height3 + ')')
        .attr("class", "axis axis--x")
        .call(this.xAxis3);

    this.timeline3.append('g')
        .attr("class", "axis axis--y")
        .call(this.yAxisLeft3);

    this.timeline3.append('g')
        .attr("class", "axis axis--y")
        .attr('transform', 'translate(' + this.width1 + ', 0)')
        .call(this.yAxisRight3);

    this.timeline3.append("text")
        .attr('class', 'axis-label')
        .attr("transform", "rotate(-90)")
        .attr("y", -50)
        .attr("x", 0 - (this.height3 / 2))
        .attr("dy", "1em")
        .style("text-anchor", "middle")
        .text("Speed km/h");

    this.timeline3.append("text")
        .attr('class', 'axis-label')
        .attr("transform", "rotate(90)")
        .attr("y", -this.width1 - 50)
        .attr("x", (this.height3 / 2))
        .attr("dy", "1em")
        .style("text-anchor", "middle")
        .text("Measurement Differences km/h");

    this.timeline3.append("text")
        .attr('class', 'axis-label')
        .attr("transform",
            "translate(" + (this.width1 / 2) + " ," +
            (this.height3 + 20) + ")")
        .attr("dy", "1em")
        .style("text-anchor", "middle")
        .text("Time");


    this.line1 = d3.line()
        .x(d => this.x1(new Date(d.datetime)))
        .y(d => this.y1(d.speed_odom));

    this.line1_2 = d3.line()
        .x(d => this.x1(new Date(d.datetime)))
        .y(d => this.y1_2(parseFloat(d.diff_opgs)));

    this.line2 = d3.line()
        .x(d => this.x2(new Date(d.datetime)))
        .y(d => this.y2(d.speed_odom));

    this.line2_2 = d3.line()
        .x(d => this.x2(new Date(d.datetime)))
        .y(d => this.y2_2(parseFloat(d.diff_opgs)));

    this.line3 = d3.line()
        .x(d => this.x3(new Date(d.datetime)))
        .y(d => this.y3(d.speed_odom));

    this.line3_2 = d3.line()
        .x(d => this.x3(new Date(d.datetime)))
        .y(d => this.y3_2(parseFloat(d.diff_opgs)))
};

TimeLine.prototype.addData = function () {
    let sessions = d3.nest()
        .key(function (d) {
            return d.global_session_id;
        })
        .entries(this.data);

    var _this = this;

    sessions.forEach(function (session) {
        _this.timeline1
            .append('path')
            .datum(session.values)
            .attr('class', 'line line--speed')
            .attr('d', d => _this.line1(d));

        _this.timeline1
            .append('path')
            .datum(session.values)
            .attr('class', 'line line--opg')
            .attr('d', d => _this.line1_2(d))
            .attr('stroke', 'url(#svgGradient)');

        _this.timeline2
            .append('path')
            .datum(session.values)
            .attr('class', 'line line--speed')
            .attr('d', d => _this.line2(d));

        _this.timeline2
            .append('path')
            .datum(session.values)
            .attr('class', 'line line--opg')
            .attr('d', d => _this.line2_2(d))
            .attr('stroke', 'url(#svgGradient)');

        _this.timeline3
            .append('path')
            .datum(session.values)
            .attr('class', 'line line--speed')
            .attr('d', d => _this.line3(d));

        _this.timeline3
            .append('path')
            .datum(session.values)
            .attr('class', 'line line--opg')
            .attr('d', d => _this.line3_2(d))
            .attr('stroke', 'url(#svgGradient)')

    })
};

TimeLine.prototype.addBrushes = function () {
    let minDate = d3.min(this.data, d => new Date(d.datetime));
    var _this = this;

    this.brush1 = d3.brushX()
        .extent([
            [0, 0],
            [this.width1, this.height1]
        ])
        .on("brush end", function () {
            let start_time = _this.x2.domain()[0];
            let start_time2 = _this.x3.domain()[0];
            let s = d3.event.selection || _this.x1.range();
            _this.x2.domain(s.map(_this.x1.invert, _this.x1));
            _this.timeline2.selectAll(".line--speed").attr("d", _this.line2);
            _this.timeline2.selectAll(".line--opg").attr("d", _this.line2_2);
            _this.timeline2.select(".axis--x").call(_this.xAxis2);

            let end_time = _this.x2.domain()[0];
            let secondOffset = d3.timeSecond.count(start_time, end_time);
            _this.x3.domain([d3.timeSecond.offset(_this.x3.domain()[0], secondOffset), d3.timeSecond.offset(_this.x3.domain()[1], secondOffset)])
            _this.timeline3.selectAll(".line--speed").attr("d", _this.line3);
            _this.timeline3.selectAll(".line--opg").attr("d", _this.line3_2);
            _this.timeline3.select(".axis--x").call(_this.xAxis3);

            let end_time2 = _this.x3.domain()[0];
            let secondOffset2 = d3.timeSecond.count(start_time2, end_time2);
            _this.x4.domain([d3.timeSecond.offset(_this.x4.domain()[0], secondOffset2), d3.timeSecond.offset(_this.x4.domain()[1], secondOffset2)])

            _this.updateDetails()
        });

    this.brush2 = d3.brushX()
        .extent([
            [0, 0],
            [this.width1, this.height2]
        ])
        .on("brush end", function () {
            let start_time = _this.x3.domain()[0];
            let s = d3.event.selection || _this.x2.range();
            _this.x3.domain(s.map(_this.x2.invert, _this.x2));
            _this.timeline3.selectAll(".line--speed").attr("d", _this.line3);
            _this.timeline3.selectAll(".line--opg").attr("d", _this.line3_2);
            _this.timeline3.select(".axis--x").call(_this.xAxis3);

            let end_time = _this.x3.domain()[0];
            let secondOffset = d3.timeSecond.count(start_time, end_time);
            _this.x4.domain([d3.timeSecond.offset(_this.x4.domain()[0], secondOffset), d3.timeSecond.offset(_this.x4.domain()[1], secondOffset)])

            _this.updateDetails()
        });

    this.brush3 = d3.brushX()
        .extent([
            [0, 0],
            [this.width1, this.height3]
        ]).on("brush end", function () {
            var s = d3.event.selection || _this.x3.range();
            _this.x4.domain(s.map(_this.x3.invert, _this.x3));

            _this.updateDetails()
        });

    this.timeline1.append("g")
        .attr("class", "brush")
        .call(this.brush1).call(this.brush1.move, [0, this.x1(d3.timeMinute.offset(minDate, 30))]);

    this.timeline2.append("g")
        .attr("class", "brush")
        .call(this.brush2).call(this.brush2.move, [0, this.x2(d3.timeMinute.offset(minDate, 1))]);

    this.gBrush3 = this.timeline3.append("g")
        .attr("class", "brush")
        .call(this.brush3);

    this.gBrush3.call(this.brush3.move, [0, this.x3(d3.timeSecond.offset(minDate, 10))]);

    this.brush3Width = this.x3(d3.timeSecond.offset(minDate, 10))
};

TimeLine.prototype.updateDetails = function () {

    var filteredData = this.data.filter(d => {
        return (this.x4.domain()[0] <= new Date(d.datetime) && new Date(d.datetime) <= this.x4.domain()[1])
    })

    //    FIXME: Uncomment this
    //    this.sonify(filteredData, this)
};

TimeLine.prototype.updateOnScroll = function () {

    var _this = this;
    this.window.scroll(function () {
        if (_this.container.classed("hidden")) {
            return
        }
        if (_this.window.scrollTop() > _this.x3.range()[1]) {
            return
        }

        _this.gBrush3.call(_this.brush3.move, [_this.window.scrollTop(), _this.window.scrollTop() + _this.brush3Width])
    })
};

TimeLine.prototype.prepSound = function () {
    this.synth = new Tone.PolySynth(2, Tone.Sine).toMaster();

    this.synth.set({
        "envelope": {
            "attack": 1,
            "release": 1
        }
    });

    this.yScaleSound = d3.scaleLinear()
        .range([100, 440])
        .domain(d3.extent(this.data, d => +d.speed_odom))
        .clamp(true)

};

TimeLine.prototype.sonify = ((data, _this) => {
    let val1 = _this.yScaleSound(d3.mean(data.map(d => d.speed_odom)));
    let val2 = val1 * d3.mean(data.map(d => d.diff_opgs));

    _this.synth.triggerAttackRelease([val2, val1]);
});