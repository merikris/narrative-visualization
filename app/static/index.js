$(function () {

    var w = $(window),
        container = d3.select(".container"),
        menu = d3.select(".sidebar-nav-fixed"),
        width = container.node().getBoundingClientRect().width,
        height = w.height();

    w.scrollTop(0);

    //Wait till all data is loaded and elements created to create a controller
    Promise.all([
        d3.csv("/static/data/train_28_50_sessions2.csv"),
        d3.csv("/static/data/window_stats_downsampled.csv"),
        d3.json("static/data/histogram_boundaries.json"),
        d3.json("/static/data/trees31.json"),
        d3.json("/static/data/predictions31.json")
    ]).then(function (values) {

        var introduction = new Introduction({
            container: container.append('div').attr('class', 'episode-container').attr('id', 'intro'),
            height: height,
            width: width,
            window: w,
        });

        var timeline = new TimeLine({
            container: container.append('div').attr('class', 'episode-container').attr('id', 'timeline'),
            data: values[0],
            height: height,
            width: width,
            window: w
        });

        var histogram = new Histogram({
            container: container.append('div').attr('class', 'episode-container').attr('id', 'histogram'),
            data: values[1],
            thresholds: values[2],
            height: height,
            width: width,
            window: w,
        });

        var forest = new ForestPredictor({
            container: container.append('div').attr('class', 'episode-container').attr('id', 'forest'),
            data: values[3],
            predictions: values[4],
            height: height,
            width: width,
            window: w
        });

        var items = [{url: 'intro', title: 'Overview', element: introduction},
            {url: 'timeline', title: 'Timeline', element: timeline},
            {url: 'histogram', title: 'Histogram', element: histogram},
            {url: 'forest', title: 'Random Forest', element: forest, state: 0},
            {url: 'forest', title: 'Decision Tree', element: forest, state: 1},
            {url: 'forest', title: 'Classification', element: forest, state: 2}];

        var controller = new Controller({
            items: items,
            menu: menu,
            height:height,
            window: w
        })
    });
});